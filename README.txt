This module prevents to submit a comment if it has any offensive words.
-------------------------------
Installation:
----------------------
1. Download module from https://www.drupal.org/project/smart_comment
2. Unzip and keep this module under project/modules/contrib/
3. Enable the module from Admin panel.
4. Configure abusive words from admin admin-> configuration->Smart Comment Configuration and configure words and error message.
5. Configure the abusive words. You can put multiple words separated by comma (,)
6. You can also configure error message from admin.

Issue:

If you face any issue, please let us know
